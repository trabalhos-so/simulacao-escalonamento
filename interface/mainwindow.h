#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts>
#include <iostream>

    //--------------------------------------------
    //                  CLASSE MAINWINDOW
    //
    //  Possui metodos utilizados pelo Qt que
    //  geram o diagrama de tempo e a tabela de
    //  tempos individuais de cada processo.
    //--------------------------------------------

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QString nomeAlgoritmo;

    QValueAxis *axisY;
    QValueAxis *axisX;
    QChart *chart;
    QChartView *chartView;

    std::vector<QLineSeries*> vectorSeriesExec;
    std::vector<QLineSeries*> vectorSeriesWait;
    std::vector<QTableWidgetItem*> vectorProcessos;
public:
    //--------------------------------------------
    //                  CONSTRUTOR
    MainWindow(QWidget *parent = nullptr);
    //      Construtor padrao Qt.
    //--------------------------------------------
    //                  DESTRUTOR
    ~MainWindow();
    //      Destrutor padrao.
    //--------------------------------------------
    //                  METODO DEFINENOME
    void defineNome(int);
    //      Metodo set para o atributo nomeAlgoritmo
    //  da classe, para que o nome que aparece acima
    //  do diagrama de tempo seja correto.
    //      Argumento inteiro recebido corresponde
    //  a algum algoritmo de escalonamento.
    //--------------------------------------------
    //                  METODO GERAGRAFICO
    QChartView* geraGrafico(std::vector<std::vector<int>>);
    //      Metodo que recebe a matrizTempo da classe
    //  Escalonador e transforma as informacoes armazenadas
    //  em pontos no diagrama de tempo.
    //--------------------------------------------
    //                  METODO ADICIONAOUTPUT
    QTableWidget* adicionaOutput(std::vector<std::vector<int>>);
    //      Metodo que recebe a matriz individualOutput
    //  da classe Escalonador e transfere as informacoes para
    //  uma tabela que eh apresentada abaixo do diagrama de tempo.
    //--------------------------------------------
    //                  METODO GERAOUTPUT
    QLabel* geraLabel(std::vector<std::vector<int>>);
    //      Metodo que recebe a matriz individualOutput
    //  da classe Escalonador e transfere as informacoes de
    //  tempo total de espera e numero de troca de contextos
    //  para uma label apresentada abaixo da tabela.
    //--------------------------------------------
};
#endif // MAINWINDOW_H
