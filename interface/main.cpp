#include "../include/escalonador.hpp"
#include "../include/toolbox.hpp"
#include "mainwindow.h"
#include <QApplication>
#include <QVBoxLayout>
#include <vector>

int main(int argc, char *argv[])
{
    if(argc != 2 || std::stoi(argv[1],nullptr) < 1 || std::stoi(argv[1],nullptr) > 6)
    {
        std::cerr << "\033[0;31mExecute o programa conforme as instrucoes do arquivo README!\033[0m\n";
        return 1;
    }
    Toolbox objToolbox;
    std::vector<std::vector<int>> matriz = objToolbox.leituraMatriz("../info.txt");

    if(matriz.empty() == true) return 1;

    Escalonador objEscalonador(argv[1], matriz);
    objEscalonador.imprimeResultados(false);

    QApplication a(argc, argv);
    MainWindow w;
    w.defineNome(std::stoi(argv[1]));

    QWidget* outputWidget = new QWidget();
    w.setCentralWidget(outputWidget);

    QChartView* chartView = w.geraGrafico(objEscalonador.getMatrizTempo());
    QTableWidget* tableStacked = w.adicionaOutput(objEscalonador.getIndividualOutput());
    QLabel* labelTexto = w.geraLabel(objEscalonador.getIndividualOutput());

    QVBoxLayout *layout = new QVBoxLayout(outputWidget);
    layout->addWidget(chartView);
    layout->addWidget(tableStacked);
    layout->addWidget(labelTexto);

    outputWidget->setLayout(layout);

    w.showMaximized();
    w.show();

    return a.exec();
}
