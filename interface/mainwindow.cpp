#include "mainwindow.h"

QT_CHARTS_USE_NAMESPACE

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    nomeAlgoritmo = "\0";
}

MainWindow::~MainWindow()
{
    for(unsigned int i = 0; i < vectorSeriesExec.size(); i++) delete vectorSeriesExec[i];
    vectorSeriesExec.clear();
    for(unsigned int i = 0; i < vectorSeriesWait.size(); i++) delete vectorSeriesWait[i];
    vectorSeriesWait.clear();
    for(unsigned int i = 0; i < vectorProcessos.size(); i++) delete vectorProcessos[i];
    vectorProcessos.clear();
    delete axisX;
    delete axisY;
    delete chart;
    delete chartView;
}

QLabel* MainWindow::geraLabel(std::vector<std::vector<int>> matrizOutput)
{
    QLabel* labels = new QLabel();
    int trocasContexto = 0, totalEspera = 0;
    for(unsigned int i = 0; i < matrizOutput.size(); i++)
    {
        totalEspera += matrizOutput[i][1];
        trocasContexto += matrizOutput[i][2];
    }
    QString s("Tempo medio de espera de ");
    s.append(QString::number(matrizOutput.size()));
    s.append(" processos = ");
    s.append(QString::number(((double)totalEspera / (double)matrizOutput.size())));
    s.append(" segundos\nTrocas de contexto = ");
    s.append(QString::number(trocasContexto));
    labels->setText(s);
    QFont f("Times", 15, QFont::DemiBold);
    labels->setFont(f);
    return labels;
}

QTableWidget* MainWindow::adicionaOutput(std::vector<std::vector<int>> matrizOutput)
{
    int numeroProcessos = matrizOutput.size(), trocasContexto = 0;
    QTableWidget* tableProcessos = new QTableWidget(2, numeroProcessos);
    QStringList headerH, headerV;
    headerV << "Turnaround Time" << "Tempo espera";
    tableProcessos->setVerticalHeaderLabels(headerV);
    for(int i = 0; i < numeroProcessos; i++)
    {
        trocasContexto += matrizOutput[i][2];
        QString qs = QString::number(i+1);
        qs.insert(0,'P');
        headerH.append(qs);
        for(int j = 0; j < 2; j++)
        {
            QTableWidgetItem* item = new QTableWidgetItem;
            item->setText(QString::number(matrizOutput[i][j]));
            tableProcessos->setItem(j, i, item);
            vectorProcessos.push_back(item);
        }
    }
    tableProcessos->setHorizontalHeaderLabels(headerH);
    tableProcessos->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
    int alturaLinhas = 0;
    for (int i = 0; i < tableProcessos->verticalHeader()->count(); ++i) if (!tableProcessos->verticalHeader()->isSectionHidden(i)) alturaLinhas+=tableProcessos->verticalHeader()->sectionSize(i);
    if (!tableProcessos->horizontalScrollBar()->isHidden()) alturaLinhas+=tableProcessos->horizontalScrollBar()->height();
    if (!tableProcessos->horizontalHeader()->isHidden()) alturaLinhas+=tableProcessos->horizontalHeader()->height();
    tableProcessos->setMaximumHeight(alturaLinhas);
    return tableProcessos;
}

QChartView* MainWindow::geraGrafico(std::vector<std::vector<int>> matrizTempo)
{
    int tempoTotal = matrizTempo.size();
    int numeroProcessos = matrizTempo[0].size();

    chart = new QChart();
    chart->createDefaultAxes();
    chart->legend()->hide();
    chart->setTitle(nomeAlgoritmo);

    axisX = new QValueAxis();
    axisX->setLabelFormat("%i");
    axisX->setRange(0, tempoTotal);
    axisX->setTickCount(tempoTotal+1);
    axisX->setTitleText("Tempo (segundos)");
    chart->addAxis(axisX, Qt::AlignBottom);

    axisY = new QValueAxis();
    axisY->setLabelFormat("P%i");
    axisY->setRange(1, numeroProcessos);
    axisY->setTickCount(numeroProcessos);
    axisY->setTitleText("Processos");
    chart->addAxis(axisY, Qt::AlignLeft);

    for(int i = 0; i < tempoTotal; i++)
    {
        QLineSeries *seriesExec = new QLineSeries;
        vectorSeriesExec.push_back(seriesExec);
        QPen penExec(QRgb(0xff0000));
        penExec.setWidth(5);
        seriesExec->setPen(penExec);
        for(int j = 0; j < numeroProcessos; j++)
        {
            QLineSeries *seriesWait = new QLineSeries;
            vectorSeriesWait.push_back(seriesWait);
            QPen penWait(QRgb(0x0000ff));
            penWait.setStyle(Qt::DashLine);
            penWait.setWidth(5);
            seriesWait->setPen(penWait);
            int valorMatriz = matrizTempo[i][j];
            switch(valorMatriz)
            {
                case 1:
                    seriesWait->append(i, j+1);
                    seriesWait->append(i+1, j+1);
                    break;
                case 2:
                    seriesExec->append(i, j+1);
                    seriesExec->append(i+1, j+1);
                    break;
                default:
                    break;
            }
            chart->addSeries(seriesWait);
            seriesWait->attachAxis(axisX);
            seriesWait->attachAxis(axisY);
        }
        chart->addSeries(seriesExec);
        seriesExec->attachAxis(axisX);
        seriesExec->attachAxis(axisY);
    }
    chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    return chartView;
}

void MainWindow::defineNome(int codigo)
{
    switch (codigo)
    {
        case 1:
            nomeAlgoritmo = "First Come First Served";
            break;
        case 2:
            nomeAlgoritmo = "Shortest Job First";
            break;
        case 3:
            nomeAlgoritmo = "Prioridade Sem Preempsao";
            break;
        case 4:
            nomeAlgoritmo = "Prioridade Com Preempsao";
            break;
        case 5:
            nomeAlgoritmo = "Round Robin Sem Prioridade";
            break;
        case 6:
            nomeAlgoritmo = "Round Robin Com Prioridade e Envelhecimento";
            break;
        default:
            std::cerr << "\033[0;31mExecute o programa conforme as instrucoes do arquivo README!\033[0m\n";
            break;
    }
}
