#include "../include/toolbox.hpp"

Toolbox::Toolbox()
{}

Toolbox::~Toolbox()
{}

std::vector<std::vector<int>> Toolbox::leituraMatriz(std::string nomeArquivo)
{
    std::ifstream arquivoInfo(nomeArquivo);
    std::vector<std::vector<int>> matriz;
    if(arquivoInfo.is_open())
    {

        std::string linha;
        std::queue<std::string> queueLinhas;
        while( getline(arquivoInfo, linha) ) queueLinhas.push(linha);
        arquivoInfo.close();

        while(queueLinhas.empty() == false)
        {
            std::size_t pos = queueLinhas.front().find_first_of("0123456789");
            //LINHAS VAZIAS OU SEM NUMEROS SERAO DESCONSIDERADAS
            if(pos != std::string::npos)
            {
                std::vector<int> linhaAtual;
                while(pos != std::string::npos)
                {
                    queueLinhas.front().erase(0, pos);
                    pos = 0;
                    std::string coluna;
                    while( pos == 0 )
                    {
                        coluna.push_back(queueLinhas.front()[pos]);
                        queueLinhas.front().erase(0, pos+1);
                        pos = queueLinhas.front().find_first_of("0123456789");
                    }
                    linhaAtual.push_back(std::stoi(coluna, nullptr));
                }
                matriz.push_back(linhaAtual);
            }
            queueLinhas.pop();
        }
    } else std::cerr << "\033[0;31mErro ao abrir o arquivo texto\033[0m\n";
    return matriz;
}

void Toolbox::printMatriz(std::vector<std::vector<int>> matriz)
{
    for(unsigned int i = 0; i < matriz.size(); i++)
    {
        for(unsigned int j = 0; j < matriz[i].size(); j++)
        {
            std::cout << matriz[i][j] << " ";
        }
        std::cout << std::endl;
    }
}