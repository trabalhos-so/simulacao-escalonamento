#include "../include/escalonador.hpp"
#include "../include/toolbox.hpp"

int main(int argc, char* argv[])
{
    int param = std::stoi(argv[1],nullptr);
    if(argc != 3 || param < 1 || param > 6)
    {
        std::cerr << "\033[0;31mExecute o programa conforme as instrucoes do arquivo README!\033[0m\n";
        return 1;
    }
    Toolbox objToolbox;
    std::vector<std::vector<int>> matriz = objToolbox.leituraMatriz("info.txt");
    if(matriz.empty() == false)
    {
        bool print = true;
        if(((std::string)argv[2]).find_first_of("0") != std::string::npos) print = false;
        Escalonador objEscalonador(argv[1], matriz);
        objEscalonador.imprimeResultados(print);
    }
    return 0;
}