#include "../include/escalonador.hpp"

Escalonador::Escalonador(char* algoritmo, std::vector<std::vector<int>> matrizArg) : matrizEntrada(matrizArg)
{
    std::string tempEscolha = algoritmo;
    if( (tempEscolha.size() != 1) || (tempEscolha.find_first_of("123456") == std::string::npos) )
    {
        std::cerr << "\033[0;31mExecute o programa conforme as instrucoes do arquivo README!\033[0m\n";
    }
    int escolha = std::stoi(algoritmo, nullptr);
    tempoTotal = 0;
    for(unsigned int i = 0; i < matrizEntrada.size(); i++)  tempoTotal += matrizEntrada[i][1];
    for(int i = 0; i < tempoTotal; i++) matrizTempo.push_back(std::vector<int>(matrizEntrada.size(), NAO_CRIADO));
    switch (escolha)
    {
        case 1:
            std::cout << "Algoritmo selecionado: \033[0;32mFirst Come First Served\033[0m.\n";
            FirstComeFirstServed();
            break;
        case 2:
            std::cout << "Algoritmo selecionado: \033[0;33mShortest Job First\033[0m.\n";
            ShortestJobFirst();
            break;
        case 3:
            std::cout << "Algoritmo selecionado: \033[0;34mPor prioridade, sem preempção\033[0m.\n";
            Prioridade_Sem();
            break;
        case 4:
            std::cout << "Algoritmo selecionado: \033[1;34mPor prioridade, com preempção por prioridade\033[0m.\n";
            Prioridade_Com();
            break;
        case 5:
            std::cout << "Algoritmo selecionado: \033[0;35mRound-Robin com quantum = 2s, sem prioridade\033[0m.\n";
            RoundRobin_Sem();
            break;
        case 6:
            std::cout << "Algoritmo selecionado: \033[1;35mRound-Robin com prioridade e envelhecimento (tq=2, α=1)\033[0m.\n";
            RoundRobin_Com();
            break;
    }
    
}

Escalonador::~Escalonador()
{}

std::vector<std::vector<int>> Escalonador::getIndividualOutput()
{
    return individualOutput;
}

std::vector<std::vector<int>> Escalonador::getMatrizTempo()
{
    return matrizTempo;
}

void Escalonador::imprimeResultados(bool imprime)
{
    if(matrizTempo.empty() == true) matrizTempo.push_back(std::vector<int>(matrizEntrada.size(), 0));
    std::vector<std::vector<int>> matrizOutput((int)matrizTempo[0].size(), std::vector<int>(3, 0));
    int ultimoExecucao = -1;
    if(imprime == true)
    {
        std::cout << "tempo\t";
        for(unsigned int i = 0; i < matrizEntrada.size(); i++) std::cout << "P" << i+1 << " ";
        std::cout << std::endl;
    }
    for(unsigned int i = 0; i < matrizTempo.size(); i++)
    {
        if(imprime == true)
        {
            if(i < 10) std::cout << " ";
            std::cout << i << "-";
            if(i+1 < 10) std::cout << " ";
            std::cout << i+1 << "\t";
        }
        for(unsigned int j = 0; j < matrizTempo[i].size(); j++)
        {
            char estadoPrint = '\0';
            const char* cor = "\0";
            if((matrizTempo[i][j] == 2) && ( (ultimoExecucao != -1) && (ultimoExecucao != (int)j))) matrizOutput[j][2]++;
            switch (matrizTempo[i][j])
            {
                case 0:
                    estadoPrint = ' ';
                    cor = "\0";
                    break;
                case 1:
                    estadoPrint = '-';
                    matrizOutput[j][0]++;
                    matrizOutput[j][1]++;
                    cor = "\033[0;34m";
                    break;
                case 2:
                    estadoPrint = '#';
                    matrizOutput[j][0]++;
                    ultimoExecucao = j;
                    cor = "\033[0;31m";
                    break;
                case 3:
                    estadoPrint = ' ';
                    cor = "\0";
                    break;
            }
            if(imprime == true) std::cout << cor << estadoPrint << estadoPrint << "\033[0m ";
        }
        if(imprime == true) std::cout << std::endl;
    }
    int totalEspera = 0, trocasContexto = 0;
    if(imprime == true) std::cout << "--------------------------------\n";
    for(unsigned int i = 0; i < matrizOutput.size(); i++)
    {
        if(imprime == true) std::cout << "Processo " << i+1 << ":\n\tTurnaround Time    = " << matrizOutput[i][0] << "s\n\tTempo em espera    = " << matrizOutput[i][1] << "s\n--------------------------------\n";
        totalEspera += matrizOutput[i][1];
        trocasContexto += matrizOutput[i][2];
    }
    if(imprime == true) std::cout << "Tempo medio de espera de " << (int)matrizOutput.size() << " processos = " << ((double)totalEspera / (double)matrizOutput.size()) << " segundos\nTrocas de contexto = " << trocasContexto << std::endl;
    individualOutput = matrizOutput;
}

std::vector<int> Escalonador::organizaChegada()
{
    std::vector<int> ordemChegada;
    std::vector<std::vector<int>> matrixAux = matrizEntrada;
    unsigned int contador = 0;
    for(unsigned int i = 0; i < matrizEntrada.size(); i++) matrixAux[i].push_back(i);
    
    while(contador < matrizEntrada.size())
    {
        int menorValor = -1;
        int menorIndex;
        int posicao_menor = 0;
        for(unsigned int i = contador; i < matrixAux.size(); i++)
        {
            if((menorValor == -1) || (menorValor > matrixAux[i][0]))
            {
                menorValor = matrixAux[i][0];
                menorIndex = matrixAux[i][3];
                posicao_menor = i;
            }
        }
        ordemChegada.push_back(menorIndex);
        matrixAux.insert(matrixAux.begin() + contador, matrixAux[posicao_menor]);
        matrixAux.erase(matrixAux.begin() + posicao_menor + 1);
        contador++;
    }
    return ordemChegada;
}

void Escalonador::FirstComeFirstServed()
{
int tempo = 0, vezThread = 0, executando = 0, contExe = 0;;
    std::vector<int> criados;
    std::vector<int> temposExecucao;
    for(unsigned int i = 0; i < matrizEntrada.size(); i++){
        temposExecucao.push_back(matrizEntrada[i][1]);
    }
    while(contExe < tempoTotal)
    {
        if(tempo >= tempoTotal){
            matrizTempo.push_back(std::vector<int>(matrizEntrada.size(), NAO_CRIADO));
        }
        for(unsigned int i = 0; i < matrizEntrada.size(); i++){
            if(matrizEntrada[i][0] == tempo){
                criados.push_back(i);
                if(matrizEntrada[i][1] <= 0)criados.pop_back();
            } 
        } 
        if(criados.empty()!= true && temposExecucao[criados[0]] == 0){
            matrizTempo[tempo][criados[0]] = TERMINOU;
            criados.erase(criados.begin());
            executando = 0;            
        }
    
        if(criados.empty() != true || executando==1){
            vezThread = criados[0];            
            for(unsigned int i = 1; i < criados.size(); i++){
                matrizTempo[tempo][criados[i]] = ESPERANDO;
            }
            executando = 1;
            contExe++;
            temposExecucao[vezThread]--;
            matrizTempo[tempo][vezThread]= EXECUTANDO;
        }
        tempo++;
    }
}

void Escalonador::ShortestJobFirst()
{
    std::vector<int> ordemChegada = organizaChegada();
    int tempo = 0, idThreadOrdem = -1;
    std::vector<int> tempoThread(ordemChegada.size(), 0);
    while(tempo < tempoTotal)
    {
        if((idThreadOrdem == -1) || (matrizEntrada[ordemChegada[idThreadOrdem]][1] == tempoThread[idThreadOrdem]))
        {
            int menorTempo = -1;
            for(unsigned int i = 0; i < ordemChegada.size(); i++)
            {
                if( (matrizEntrada[ordemChegada[i]][0] <= tempo) && ( ( (matrizEntrada[ordemChegada[i]][1]-tempoThread[i]) > 0 ) && ((menorTempo == -1) || ((matrizEntrada[ordemChegada[i]][1]-tempoThread[i]) < menorTempo))))
                {
                    idThreadOrdem = i;
                    menorTempo = matrizEntrada[ordemChegada[i]][1];
                }
            }
        }
        if(idThreadOrdem == -1)
        {
            tempoTotal++;
            matrizTempo.push_back(std::vector<int>(matrizEntrada.size(), NAO_CRIADO));
        }
        for(unsigned int i = 0; i < matrizEntrada.size(); i++)
        {
            if((tempo > 0) && (matrizTempo[tempo-1][i] == TERMINOU)) matrizTempo[tempo][i] = TERMINOU;
            if(matrizTempo[tempo][i] != TERMINOU)
            {
                if(matrizEntrada[i][0] <= tempo) matrizTempo[tempo][i] = ESPERANDO;
                if((idThreadOrdem != -1) && ((unsigned int)(ordemChegada[idThreadOrdem]) == i) )
                {
                    matrizTempo[tempo][i] = EXECUTANDO;
                    tempoThread[idThreadOrdem]++;
                }
                if(matrizEntrada[i][1] < 1) matrizTempo[tempo][i] = NAO_CRIADO;
            }
        }
        tempo++;
        if((idThreadOrdem != -1) && (matrizEntrada[ordemChegada[idThreadOrdem]][1] == tempoThread[idThreadOrdem]) && (tempo < tempoTotal))
        {
            matrizTempo[tempo][ordemChegada[idThreadOrdem]] = TERMINOU;
            idThreadOrdem = -1;
        }
    }
}

void Escalonador::Prioridade_Sem()
{
    std::vector<int> ordemChegada = organizaChegada();
    int tempo = 0, idThreadOrdem = -1;
    std::vector<int> tempoThread(ordemChegada.size(), 0);
    while(tempo < tempoTotal)
    {
        if((idThreadOrdem == -1) || (matrizEntrada[ordemChegada[idThreadOrdem]][1] == tempoThread[idThreadOrdem]))
        {
            int maiorPrioridade = -1;
            for(unsigned int i = 0; i < ordemChegada.size(); i++)
            {
                if( (matrizEntrada[ordemChegada[i]][0] <= tempo) && ( ( (matrizEntrada[ordemChegada[i]][1]-tempoThread[i]) > 0 ) && ((maiorPrioridade == -1) || (matrizEntrada[ordemChegada[i]][2] > maiorPrioridade))))
                {
                    idThreadOrdem = i;
                    maiorPrioridade = matrizEntrada[ordemChegada[i]][2];
                }
            }
        }
        if(idThreadOrdem == -1)
        {
            tempoTotal++;
            matrizTempo.push_back(std::vector<int>(matrizEntrada.size(), NAO_CRIADO));
        }
        for(unsigned int i = 0; i < matrizEntrada.size(); i++)
        {
            if((tempo > 0) && (matrizTempo[tempo-1][i] == TERMINOU)) matrizTempo[tempo][i] = TERMINOU;
            if(matrizTempo[tempo][i] != TERMINOU)
            {
                if(matrizEntrada[i][0] <= tempo) matrizTempo[tempo][i] = ESPERANDO;
                if((idThreadOrdem != -1) && ((unsigned int)(ordemChegada[idThreadOrdem]) == i) )
                {
                    matrizTempo[tempo][i] = EXECUTANDO;
                    tempoThread[idThreadOrdem]++;
                }
                if(matrizEntrada[i][1] < 1) matrizTempo[tempo][i] = NAO_CRIADO;
            }
        }
        tempo++;
        if((idThreadOrdem != -1) && (matrizEntrada[ordemChegada[idThreadOrdem]][1] == tempoThread[idThreadOrdem]) && (tempo < tempoTotal))
        {
            matrizTempo[tempo][ordemChegada[idThreadOrdem]] = TERMINOU;
            idThreadOrdem = -1;
        }
    }
}

void Escalonador::Prioridade_Com()
{
    std::vector<int> ordemChegada = organizaChegada();
    int tempo = 0, idThreadOrdem = -1;
    std::vector<int> tempoThread(ordemChegada.size(), 0);
    while(tempo < tempoTotal)
    {
        int maiorPrioridade = -1;
        for(unsigned int i = 0; i < ordemChegada.size(); i++)
        {
            if( (matrizEntrada[ordemChegada[i]][0] <= tempo) && ( ( (matrizEntrada[ordemChegada[i]][1]-tempoThread[i]) > 0 ) && ((maiorPrioridade == -1) || (matrizEntrada[ordemChegada[i]][2] > maiorPrioridade))))
            {
                idThreadOrdem = i;
                maiorPrioridade = matrizEntrada[ordemChegada[i]][2];
            }
        }
        if(idThreadOrdem == -1)
        {
            tempoTotal++;
            matrizTempo.push_back(std::vector<int>(matrizEntrada.size(), NAO_CRIADO));
        }
        for(unsigned int i = 0; i < matrizEntrada.size(); i++)
        {
            if((tempo > 0) && (matrizTempo[tempo-1][i] == TERMINOU)) matrizTempo[tempo][i] = TERMINOU;
            if(matrizTempo[tempo][i] != TERMINOU)
            {
                if(matrizEntrada[i][0] <= tempo) matrizTempo[tempo][i] = ESPERANDO;
                if((idThreadOrdem != -1) && ((unsigned int)(ordemChegada[idThreadOrdem]) == i) )
                {
                    matrizTempo[tempo][i] = EXECUTANDO;
                    tempoThread[idThreadOrdem]++;
                }
                if(matrizEntrada[i][1] < 1) matrizTempo[tempo][i] = NAO_CRIADO;
            }
        }
        tempo++;
        if((idThreadOrdem != -1) && (matrizEntrada[ordemChegada[idThreadOrdem]][1] == tempoThread[idThreadOrdem]) && (tempo < tempoTotal))
        {
            matrizTempo[tempo][ordemChegada[idThreadOrdem]] = TERMINOU;
            idThreadOrdem = -1;
        }
    }
}

void Escalonador::RoundRobin_Sem()
{
    int tempo = 0, vezThread = 0, aux;
    std::vector<int> criados;
    std::vector<int> temposExecucao;
    for(unsigned int i = 0; i < matrizEntrada.size(); i++){
        temposExecucao.push_back(matrizEntrada[i][1]);
    }
    int executando = 0, contExe = 0, quantum = 0;
    while(contExe < tempoTotal)
    {
        if(tempo >= tempoTotal){
            matrizTempo.push_back(std::vector<int>(matrizEntrada.size(), NAO_CRIADO));
        }
        for(unsigned int i = 0; i < matrizEntrada.size(); i++){
            if(matrizEntrada[i][0] == tempo){
                criados.push_back(i);
                if(matrizEntrada[i][1] <= 0)criados.pop_back();
            } 
        } 
        if((criados.empty() != true)){
            if((temposExecucao[criados[0]] <= 0) || (quantum == 2)){
                aux = criados[0];
                criados.erase(criados.begin());
                if(temposExecucao[aux] > 0){
                    criados.push_back(aux);
                }else{
                    matrizTempo[tempo][aux] = TERMINOU;
                    executando = 0;
                }
                quantum = 0;
            }
        }
        if(criados.empty() != true || executando==1){
            
            vezThread = criados[0];  
            for(unsigned int i = 1; i < criados.size(); i++){
                matrizTempo[tempo][criados[i]] = ESPERANDO;
            }
            executando = 1;
            contExe++;
            temposExecucao[vezThread]--;
            matrizTempo[tempo][vezThread]= EXECUTANDO;
            quantum++;
        }
        tempo++;
    }
}

void Escalonador::RoundRobin_Com()
{
    int tempo = 0, vezThread = 0, executando = 0, contExe = 0, quantum = 2, maior = -1, posicao = 0;
    std::vector<int> criados;
    std::vector<int> temposExecucao;
    std::vector<int> prioridades;
    std::vector<int> envelhecimento;

    for(unsigned int i = 0; i < matrizEntrada.size(); i++)
    {
        temposExecucao.push_back(matrizEntrada[i][1]);
        prioridades.push_back(matrizEntrada[i][2]);
        envelhecimento.push_back(0);
    }
    
    while(contExe < tempoTotal)
    {
        if(tempo >= tempoTotal) matrizTempo.push_back(std::vector<int>(matrizEntrada.size(), NAO_CRIADO));
        maior = -1;
        if(temposExecucao[vezThread] == 0 && (criados.empty() != true))
        {
            matrizTempo[tempo][vezThread] = TERMINOU;
            for(unsigned int i = 0; i < criados.size(); i++)if(criados[i] == vezThread)posicao = i;
            criados.erase(criados.begin() + posicao);
            prioridades[vezThread] = -1;
            quantum = 0;
            executando = 0;
        }
        for(unsigned int i = 0; i < matrizEntrada.size(); i++){
            if(matrizEntrada[i][0] == tempo){
                if(executando == 1){
                    for(unsigned int p = 0; p < criados.size(); p++)if(criados[p] == vezThread)posicao = p;
                    criados.erase(criados.begin() + posicao);
                    criados.push_back(i);
                    if(matrizEntrada[i][1] <= 0)criados.pop_back();
                    criados.push_back(vezThread);
                }else {
                    criados.push_back(i);
                    if(matrizEntrada[i][1] <= 0)criados.pop_back();
                }
            } 
        } 
        if(criados.empty() != true || executando == 1)
        {
            if((quantum == 2) || (temposExecucao[vezThread] <= 0))
            {
                quantum = 0;
                for(unsigned int i = 0; i < criados.size(); i++)
                {
                    if(prioridades[criados[i]] > maior)
                    {
                        maior = prioridades[criados[i]];
                        vezThread = criados[i];
                        posicao = i;
                    }
                }
                criados.erase(criados.begin() + posicao);
                criados.push_back(vezThread);
                for(unsigned int i = 0; i < criados.size(); i++)
                {
                    if(criados[i] != vezThread)
                    {
                        prioridades[criados[i]]++;
                    }
                }
                prioridades[vezThread] = matrizEntrada[vezThread][2];
            }   
            for(unsigned int i = 0; i < criados.size(); i++)
            {
                if(criados[i] != vezThread)
                {
                    matrizTempo[tempo][criados[i]] = ESPERANDO;
                }
            }
            executando = 1;
            contExe++;
            matrizTempo[tempo][vezThread] = EXECUTANDO;
            temposExecucao[vezThread]--;
            quantum++;
        }
        tempo++;
    }
}