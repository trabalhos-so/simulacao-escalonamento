# Simulação de Algoritmos de Escalonamento

Programa que tem por objetivo simular o escalonamento de threads pelo uso de algoritmos de escalonamentos de processos;<br>
As informações para criação dos processos devem ser informadas em um arquivo "info.txt".

# Alunos

* Bernardo Dalfovo de Souza - 18204849
* Vinicius Slovinski        - 18201356

# Algoritmos suportados

* FCFS (First Come, First Served)
* Shortest Job First
* Por prioridade, sem preempção
* Por prioridade, com preempção por prioridade
* Round-Robin com quantum = 2s, sem prioridade
* Round-Robin com prioridade e envelhecimento (tq=2, α=1)

# Formato das informações de criação de processos

* Cada linha do arquivo representa um processo;
* A divisória de informações é feita utilizando um espaço em branco;
* As informações devem ser apresentadas na seguinte ordem:
    * Data de criação do processo;
    * Duração em segundos;
    * Prioridade estática (positiva, exemplo: prioridade 3 > prioridade 2);
* Exemplo:
    * P1 : Data = 0 / Duração = 5s / Prioridade = 2
    * P2 : Data = 0 / Duração = 2s / Prioridade = 3
    * P3 : Data = 1 / Duração = 4s / Prioridade = 1
    * P4 : Data = 3 / Duração = 3s / Prioridade = 4
    ```
    0 5 2
    0 2 3
    1 4 1
    3 3 4
    ```

# Output

* Turnaround time: Tempo decorrido entre a criação do processo até o momento que termina sua execução;
* Tempo médio de espera;
* Quantidade de trocas de contexto;
* Diagrama de tempo:
    * Cada linha representa um segundo da execução;
    * “##” : Processo em execução;
    * “--” : Processo criado e em espera;
    * “  “ : Processo não criado ou já terminado.
    * Diagrama do exemplo acima:
        ```
        tempo   P1 P2 P3 P4 
         0- 1   ## --      
         1- 2   ## -- --   
         2- 3   -- ## --   
         3- 4   -- ## -- --
         4- 5   --    ## --
         5- 6   --    ## --
         6- 7   ##    -- --
         7- 8   ##    -- --
         8- 9   --    -- ##
         9-10   --    -- ##
        10-11   --    ## --
        11-12   --    ## --
        12-13   ##       --
        13-14            ##
        ```

# Como compilar:

* Instale as dependências pelo terminal com o comando:

```bash
sudo apt install make g++
```

* Digite ```make``` no terminal quando estiver na pasta do projeto.

# Como executar:

* ```make fcfs``` : Executa o algoritmo de escalonamento First Come First Served;

* ```make sjf``` : Executa o algoritmo de escalonamento Shortest Job First;

* ```make prioridade_sem``` : Executa o algoritmo de escalonamento por prioridade, sem preempção;

* ```make prioridade_com``` : Executa o algoritmo de escalonamento por prioridade, com preempção por prioridade;

* ```make rr_sem``` : Executa o algoritmo de escalonamento Round-Robin com quantum = 2s, sem prioridade;

* ```make rr_com``` : Executa o algoritmo de escalonamento Round-Robin com prioridade e envelhecimento (tq=2, α=1);

# Outros comandos "make":

* ```make clean``` : Limpa os arquivos executaveis e objetos;

* ```make remake``` : Limpa os arquivos executaveis e objetos, compilando novamente em seguida;

* ```make valgrind``` : Compila o projeto e roda o valgrind para verificar vazamento de memoria em todos os algoritmos de escalonamentos mencionados acima, sem output.

# Como executar com interface grafica (Linux):

* Instale as dependências pelo terminal com o comando:

```bash
sudo apt install qt5-default libqt5charts5-dev
```

* Entre na pasta "interface":

```bash
cd interface
```

* Execute o QMake:

```bash
qmake
```

* Execute o Makefile gerado:

```bash
make
```

* Execute o programa com o número representante do algoritmo de escalonamento:

    * First Come First Served: ```./simulacao 1```
    * Shortest Job First: ```./simulacao 2```
    * Por prioridade, sem preempção: ```./simulacao 3```
    * Por prioridade, com preempção por prioridade: ```./simulacao 4```
    * Round-Robin com quantum = 2s, sem prioridade: ```./simulacao 5```
    * Round-Robin com prioridade e envelhecimento (tq=2, α=1): ```./simulacao 6```
