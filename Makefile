PROJ=escalonamento
SRC=$(shell find ./src/ -name "*.cpp" -type f)
$(shell mkdir -p objects)
OBJ=$(patsubst ./src%, ./objects%, $(patsubst %.cpp, %.o , $(SRC)))
FLAGS=-Wall -Wextra -Wshadow -Werror -I include

$(PROJ): $(OBJ)
	g++ -o $@ $^ $(FLAGS)

objects/%.o: src/%.cpp
	g++ -c -o $@ $^ $(FLAGS)

fcfs:
	./$(PROJ) 1 1
sjf:
	./$(PROJ) 2 1
prioridade_sem:
	./$(PROJ) 3 1
prioridade_com:
	./$(PROJ) 4 1
rr_sem:
	./$(PROJ) 5 1
rr_com:
	./$(PROJ) 6 1

clean:
	@find . -type f -name "*.o" -exec rm '{}' \;
	@find . -type f -name "$(PROJ)" -exec rm '{}' \;
	@rmdir objects;

remake:
	$(MAKE) clean
	$(MAKE)

valgrind:
	$(MAKE)
	valgrind --leak-check=full --show-leak-kinds=all ./$(PROJ) 1 0
	valgrind --leak-check=full --show-leak-kinds=all ./$(PROJ) 2 0
	valgrind --leak-check=full --show-leak-kinds=all ./$(PROJ) 3 0
	valgrind --leak-check=full --show-leak-kinds=all ./$(PROJ) 4 0
	valgrind --leak-check=full --show-leak-kinds=all ./$(PROJ) 5 0
	valgrind --leak-check=full --show-leak-kinds=all ./$(PROJ) 6 0