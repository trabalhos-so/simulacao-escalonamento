#ifndef TOOLBOX_HPP
#define TOOLBOX_HPP

#include <vector>
#include <queue>
#include <iostream>

//INCLUDES PARA LER O ARQUIVO TEXTO
#include <fstream>
#include <string>

        //--------------------------------------------
        //                  CLASSE TOOLBOX
        //
        //  Possui metodos utilizados pela main que
        //  nao envolvem os algoritmos de escalonamento
        //  diretamente.
        //--------------------------------------------

class Toolbox
{
    public:
        //--------------------------------------------
        //                  CONSTRUTOR
        Toolbox();
        //  Construtor padrao.
        //--------------------------------------------
        //                  DESTRUTOR
        ~Toolbox();
        //  Destrutor padrao.
        //--------------------------------------------
        //              METODO LEITURAMATRIZ
        std::vector<std::vector<int>> leituraMatriz(std::string);
        //  Realiza a leitura e blindagem do arquivo txt.
        //  Retorna uma matriz de inteiros na qual:
        //      Cada linha da matriz representa um processo;
        //      A primeira coluna representa em qual tempo
        //  o processo eh criado;
        //      A segunda coluna representa o tempo de
        //  execucao do processo;
        //      A terceira coluna representa a prioridade
        //  de execucao do processo.
        //--------------------------------------------
        //              METODO PRINTMATRIZ
        void printMatriz(std::vector<std::vector<int>>);
        //  Imprime a matriz lida pelo metodo leituraMatriz.
        //  Utilizado durante o debug.
        //--------------------------------------------

};

#endif