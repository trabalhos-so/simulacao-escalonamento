#ifndef ESCALONADOR_HPP
#define ESCALONADOR_HPP

#include <queue>
#include <string>
#include <iostream>
#include <vector>

        //--------------------------------------------
        //              CLASSE ESCALONADOR
        //
        //  Interpreta a selecao de algoritmo;
        //  Executa o algoritmo selecionado;
        //  Imprime os resultados da execucao.
        //--------------------------------------------

class Escalonador
{
    public:
        //--------------------------------------------
        //                  CONSTRUTOR
        Escalonador(char*, std::vector<std::vector<int>>);
        //      Realiza a leitura dos argumentos para 
        //  selecao do algoritmo de escalonamento 
        //  desejado pelo usuario e inicializa os
        //  atributos de classe.
        //--------------------------------------------
        //                  DESTRUTOR
        ~Escalonador();
        //      Destrutor padrao.
        //--------------------------------------------
        //          METODO IMPRIMERESULTADOS
        void imprimeResultados(bool);
        //      Metodo que imprime os resultados da
        //  execucao do algoritmo de escalonamento.
        //      Linha do tempo de todos os processos, 
        //  turnaround time, tempo medio de espera e 
        //  numero de trocas de contexto.
        //      Parametro booleano que informa o metodo
        //  se deve realizar a impressao no terminal.
        //--------------------------------------------
        //          METODO GETMATRIZTEMPO
        std::vector<std::vector<int>> getMatrizTempo();
        //      Metodo GET que retorna a matriz que 
        //  representa a linha de tempo de execucao
        //  de todos os processos.
        //      Cada linha dessa matriz representa
        //  um segundo.
        //      Cada coluna representa um processo.
        //--------------------------------------------
        //          METODO GETINDIVIDIUALOUTPUT
        std::vector<std::vector<int>> getIndividualOutput();
        //      Metodo GET que retorna uma matriz com
        //  as informacoes individuais dos processos.
        //      Cada linha dessa matriz representa 
        //  um processo.
        //      A primeira coluna representa o turnaround
        //  time, tempo decorrido entre a criacao e
        //  finalizacao do processo.
        //      A segunda coluna representa a soma de todos
        //  os segundos que o processo ficou em espera.
        //      A terceira coluna representa a quantidade
        //  de trocas de contexto realizadas pelo processo.
    private:
        //--------------------------------------------
        //          METODO FIRSTCOMEFIRSTSERVED
        void FirstComeFirstServed();
        //      Prioriza processos por ordem de chegada.
        //  Os processos que sao iniciados entram no fim
        //  da fila "criados". 
        //      O estado do primeiro da fila criados sera sempre 
        //  EXECUTANDO e dos demais ESPERANDO.
        //      Quando o tempo de execucao de um processo for 
        //  concluido, esse processo sera retirado da fila
        //  "criados" e seu estado sera TERMINOU.
        //      Entao existira um novo primeiro
        //  da fila que sera executado e assim por diante 
        //  ate todos os processos executarem.
        //--------------------------------------------
        //          METODO SHORTESTJOBFIRST
        void ShortestJobFirst();
        //      Prioriza tempos de execucao menores.
        //      Compara os tempos de execucao presentes
        //  na matriz de entrada e escolhe o processo
        //  que possui o menor tempo de execucao.
        //      Tambem utiliza o vetor com as
        //  identificacoes organizadas por ordem de
        //  chegada quando ha um "empate".
        //--------------------------------------------
        //          METODO PRIORIDADE_SEM
        void Prioridade_Sem();
        //      Prioriza processos com maior numero de
        //  prioridade, sem interromper execucoes.
        //      Analisa a matriz de entrada e encontra o
        //  processo com o maior valor de prioridade
        //  que ja esta criado, executando-o ate acabar
        //  seu tempo de execucao (sem preempsao).
        //      Quando ha um empate o vetor de identificacoes
        //  organizadas por ordem de chegada tambem eh
        //  utilizado.
        //--------------------------------------------
        //          METODO PRIORIDADE_COM
        void Prioridade_Com();
        //      Prioriza processos com maior numero de
        //  prioridade, com a possibilidade de
        //  interromper execucoes.
        //      Analisa a matriz de entrada e encontra o
        //  processo com o maior valor de prioridade
        //  que ja esta criado, caso um processo com
        //  prioridade maior que o que esta em execucao
        //  seja criado, a execucao passa para este processo
        //  (com preempsao).
        //      Quando ha um empate o vetor de identificacoes
        //  organizadas por ordem de chegada tambem eh
        //  utilizado.
        //--------------------------------------------
        //          METODO ROUNDROBIN_SEM
        void RoundRobin_Sem();
        //      Prioriza processos por ordem de chegada,
        //  porem somente executa-os pelo tempo determinado
        //  pelo quantum.
        //      O primeiro processo da fila "criados" ao ser 
        //  executado pelo tempo do quantum, vai para o final 
        //  da fila caso ainda tenha tempo de execucao a ser 
        //  realizado. O mesmo procedimento se repete para o 
        //  novo primeiro da fila ate que todos tenham 
        //  terminado seu tempo.
        //      O estado do primeiro da fila criados sera sempre 
        //  EXECUTANDO e dos demais ESPERANDO.
        //      Quando o tempo de execucao de um processo for 
        //  concluido, esse processo sera retirado da fila
        //  "criados" e seu estado sera TERMINOU.
        //--------------------------------------------
        //          METODO ROUNDROBIN_COM
        void RoundRobin_Com();
        //      Prioriza processos por prioridade e em caso 
        //  de empate dentro da fila "criados" o que vem primeiro
        //  na fila tem a preferencia, assim confirmando a 
        //  logica  Round-Robin.
        //      O detalhe e que nesse metodo ha o envelhecimento,
        //  enquanto um processo esta em execucao, os outros que 
        //  estao esperando "envelhecem", ou seja, aumentam sua 
        //  prioridade dinamica, o envelhecimento ocorre toda vez em
        //  que um processo ira assumir a CPU. Quando o escalonador 
        //  escolhe alguem aumenta a prioridade de todos que estao
        //  esperando. Alem disso, o processo que for escolhido 
        //  tera sua prioridade dinamica zerada e contara somente
        //  com sua prioridade inicial estatica.
        //      A leitura da fila ocorre, ao encontrar o processo
        //  da vez, este sera retirado e colocado no fim da fila 
        //  e seu estado sera EXECUTANDO. 
        //      Quando o tempo de execucao de um processo for 
        //  concluido, esse processo sera retirado da fila
        //  "criados" e seu estado sera TERMINOU.
        //--------------------------------------------
        //          METODO ORGANIZACHEGADA
        std::vector<int> organizaChegada();
        //      Organiza a matriz de entrada por ordem de chegada.
        //      Retorna um vetor com os ids de cada thread
        //  ordenados por chegada.
        //--------------------------------------------

        enum estados {NAO_CRIADO, ESPERANDO, EXECUTANDO, TERMINOU};

        int tempoTotal;

        const std::vector<std::vector<int>> matrizEntrada;
        std::vector<std::vector<int>> matrizTempo;
        std::vector<std::vector<int>> individualOutput;
};

#endif